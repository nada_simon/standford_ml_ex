function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%

size(X); % 300 * 2
size(centroids); % 3 * 2
m = size(X,1);
for i = 1:m
    X_repeate = ones(size(centroids, 1), 1) * X(i,:); %重复X[i],从1*2变为3*2
    distance = X_repeate - centroids;%与中心点相减
    distance = distance.^2; %平方
    sum_distance = sum(distance'); %求和
%    idx(i) = find(sum_distance == min(sum_distance));%找出坐标值 错误写法，对于浮点数，等于可能不精确，无法做到相等
    [ymin,idx(i)] = min(sum_distance);
end


% =============================================================

end

